//
// Created by liuso on 2020/4/6.
//

void ary_copy(int *dest, int *src, int len) {
  for (int i = 0; i < len; ++i) {
    dest[i] = src[i];
  }
}