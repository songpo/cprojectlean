//
// Created by liuso on 2020/4/6.
//

#ifndef _ARRAY_UTIL_H_
#define _ARRAY_UTIL_H_

#include <stdio.h>

/**
 * @brief 获取数组长度
 */
#define ary_len(ary) sizeof(ary) / sizeof(*ary)

/**
 * @brief 打印数组
 * @param ary 数组
 * @param len 数组长度
 */
#define ary_print(ary, len)                                                    \
  for (int i = 0; i < len; ++i) {                                              \
    printf_s("%d ", ary[i]);                                                   \
  }                                                                            \
  printf_s("\n")

/**
 * @brief 数组复制
 * @param dest 目标数组
 * @param src 来源数组
 * @param len  长度
 */
void ary_copy(int dest[], int src[], int len);

#endif // _ARRAY_UTIL_H_
