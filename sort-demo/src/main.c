//
// Created by liuso on 2020/4/6.
//
#include "arrayutil.h"
#include "sort.h"
#include <stdio.h>
#include <windows.h>

int main(int argc, char *argv[]) {
  // 处理控制台中文乱码
  system("chcp 65001 > null");

  int ary[] = {1, 3, 7, 4, 6, 5, 2, 9, 8};
  int len = ary_len(ary);

  // 声明并初始化临时数组
  int temp_ary[] = {};
  memset(temp_ary, 0, sizeof(temp_ary));

  // 复制ary数组的内容到temp_ary
  ary_copy(temp_ary, ary, len);
  // 打印数组内容
  ary_print(temp_ary, len);
  // 冒泡排序-从小到大
  bubble_sort_asc(temp_ary, len);
  // 打印数组内容
  ary_print(temp_ary, len);

  // 复制ary数组的内容到temp_ary
  ary_copy(temp_ary, ary, len);
  // 打印数组内容
  ary_print(temp_ary, len);
  // 冒泡排序-从大到小
  bubble_sort_desc(temp_ary, len);
  // 打印数组内容
  ary_print(temp_ary, len);

  // 复制ary数组的内容到temp_ary
  ary_copy(temp_ary, ary, len);
  // 打印数组内容
  ary_print(temp_ary, len);
  // 选择排序-从小到大
  selection_sort_asc(temp_ary, len);
  // 打印数组内容
  ary_print(temp_ary, len);

  // 复制ary数组的内容到temp_ary
  ary_copy(temp_ary, ary, len);
  // 打印数组内容
  ary_print(temp_ary, len);
  // 选择排序-从大到小
  selection_sort_desc(temp_ary, len);
  // 打印数组内容
  ary_print(temp_ary, len);

  // 复制ary数组的内容到temp_ary
  ary_copy(temp_ary, ary, len);
  // 打印数组内容
  ary_print(temp_ary, len);
  // 插入排序-从小到大
  insertion_sort_asc(temp_ary, len);
  // 打印数组内容
  ary_print(temp_ary, len);

  // 复制ary数组的内容到temp_ary
  ary_copy(temp_ary, ary, len);
  // 打印数组内容
  ary_print(temp_ary, len);
  // 插入排序-从大到小
  insertion_sort_desc(temp_ary, len);
  // 打印数组内容
  ary_print(temp_ary, len);

  return 0;
}
