//
// Created by liuso on 2020/4/6.
//

void bubble_sort_asc(int ary[], int len) {
  for (int i = 0; i < len; ++i) {
    for (int j = i + 1; j < len; ++j) {
      if (ary[j] < ary[i]) {
        int temp = ary[j];
        ary[j] = ary[i];
        ary[i] = temp;
      }
    }
  }
}

void bubble_sort_desc(int ary[], int len) {
  for (int i = 0; i < len; ++i) {
    for (int j = i + 1; j < len; ++j) {
      if (ary[j] > ary[i]) {
        int temp = ary[j];
        ary[j] = ary[i];
        ary[i] = temp;
      }
    }
  }
}

void selection_sort_asc(int *ary, int len) {
  for (int i = 0; i < len; ++i) {
    int min = ary[i];
    for (int j = i + 1; j < len; ++j) {
      if (ary[j] < min) {
        min = ary[j];
        ary[j] = ary[i];
        ary[i] = min;
      }
    }
  }
}

void selection_sort_desc(int *ary, int len) {
  for (int i = 0; i < len; ++i) {
    int min = ary[i];
    for (int j = i + 1; j < len; ++j) {
      if (ary[j] > min) {
        min = ary[j];
        ary[j] = ary[i];
        ary[i] = min;
      }
    }
  }
}

void insertion_sort_asc(int *ary, int len) {
  int i, j, temp;
  for (i = 1; i < len; i++) {
    temp = ary[i];
    for (j = i; j > 0 && ary[j - 1] > temp; j--) {
      ary[j] = ary[j - 1];
    }
    ary[j] = temp;
  }
}

void insertion_sort_desc(int *ary, int len) {
  int i, j, temp;
  for (i = 1; i < len; i++) {
    temp = ary[i];
    for (j = i; j > 0 && ary[j - 1] < temp; j--)
      ary[j] = ary[j - 1];
    ary[j] = temp;
  }
}