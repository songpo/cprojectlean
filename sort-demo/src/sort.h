//
// Created by liuso on 2020/4/6.
//

#ifndef _SORT_H_
#define _SORT_H_

/**
 * @brief 冒泡排序（英语：Bubble Sort）是一种简单的排序算法。
 * 它重复地走访过要排序的数列，一次比较两个元素，
 * 如果他们的顺序（如从小到大、首字母从A到Z）错误就把他们交换过来。
 * @param ary 数组
 * @param len 数组长度
 */
void bubble_sort_asc(int ary[], int len);

/**
 * @brief 冒泡排序（英语：Bubble Sort）是一种简单的排序算法。
 * 它重复地走访过要排序的数列，一次比较两个元素，
 * 如果他们的顺序（如从大到小、首字母从Z到A）错误就把他们交换过来。
 * @param ary 数组
 * @param len 数组长度
 */
void bubble_sort_desc(int ary[], int len);

/**
 * @brief 选择排序（Selection sort）是一种简单直观的排序算法。它的工作原理如下。
 * 首先在未排序序列中找到最小元素，存放到排序序列的起始位置，
 * 然后，再从剩余未排序元素中继续寻找最小元素，然后放到已排序序列的末尾。
 * 以此类推，直到所有元素均排序完毕。
 * @param ary 数组
 * @param len 数组长度
 */
void selection_sort_asc(int ary[], int len);

/**
 * @brief 选择排序（Selection sort）是一种简单直观的排序算法。它的工作原理如下。
 * 首先在未排序序列中找到最大元素，存放到排序序列的起始位置，
 * 然后，再从剩余未排序元素中继续寻找最大元素，然后放到已排序序列的末尾。
 * 以此类推，直到所有元素均排序完毕。
 * @param ary 数组
 * @param len 数组长度
 */
void selection_sort_desc(int ary[], int len);

/**
 * @brief 插入排序（英语：Insertion Sort）是一种简单直观的排序算法。
 * 它的工作原理是通过构建有序序列，对于未排序数据，在已排序序列中从后向前扫描，
 * 找到相应位置并插入。插入排序在实现上，通常采用in-place排序（
 * 即只需用到 {\displaystyle O(1)} {\displaystyle O(1)}的额外空间的排序），
 * 因而在从后向前扫描过程中，需要反复把已排序元素逐步向后挪位，为最新元素提供插入空间。
 * @param ary 数组
 * @param len 数组长度
 */
void insertion_sort_asc(int ary[], int len);

/**
 * @brief 插入排序（英语：Insertion Sort）是一种简单直观的排序算法。
 * 它的工作原理是通过构建有序序列，对于未排序数据，在已排序序列中从后向前扫描，
 * 找到相应位置并插入。插入排序在实现上，通常采用in-place排序（
 * 即只需用到 {\displaystyle O(1)} {\displaystyle O(1)}的额外空间的排序），
 * 因而在从后向前扫描过程中，需要反复把已排序元素逐步向后挪位，为最新元素提供插入空间。
 * @param ary 数组
 * @param len 数组长度
 */
void insertion_sort_desc(int ary[], int len);

#endif // _SORT_H_
